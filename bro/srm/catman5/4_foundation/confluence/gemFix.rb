# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

## GEM FIX
class CustomPageObject < ConfluenceClient
  
  attr_reader :title, :id, :version, :status, :created, :created_by, :last_updated

  def initialize(id,token)
    @title, @id, @version, @status, @created, @created_by, @last_updated, @url = get_by_id_body(id.to_s,token)
  end
      
  def get_by_id_body(id, token)
      
    get = RestClient::Request.execute( method:  :get,
                              url: "#{@@conf_url}/#{@@urn}/#{id}?expand=body.storage,version",
                              headers: {"Authorization" => "Bearer #{token}" },
                              :verify_ssl => false
                            )
    res = JSON.parse(get)
    res
  end        
  
  def updateBody(id, params)
    response = conn.put do |req|
      req.url "/rest/api/content/#{id}"
      req.headers['Content-Type'] = 'application/json'
      req.body = params.to_json
    end
    JSON.parse(response.body)
  end      
end      
