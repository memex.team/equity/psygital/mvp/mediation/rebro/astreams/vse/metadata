# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded    
    module Design::Сables
      class Transpose
        class << self

          ## PUBLIC
          def diff__db__confluence hIn
            dbModel = ReBro::Microservices::Archestry::Vsm::Catman5.callDbJsonProcedures(argsIn: hIn[:params], procName: 'export_status_model')[:result]
            hIn[:params][:shape] = "db"
            confluenceModel = ReBro::Microservices::Archestry::Vsm::Catman5::Transpose.transpose__confluence__db(params: hIn[:params])[:result]

            hDb = {}
            dbModel['nodes'].each do |node|
              hDb[node['type']] ||= {}
              hDb[node['type']][node['name'].to_s.strip] ||= node['description'].to_s.strip
            end
            
            hCon = {}
            confluenceModel[:nodes].each do |node|
              hCon[node['type']] ||= {}
              hCon[node['type']][node['name'].to_s.strip] ||= node['description'].to_s.strip
            end       
            
            diffFull = ::HashDiffer::Comparison.new( hDb, hCon )
            
            diff = {right: diffFull.right, left: diffFull.left, diffRight: diffFull.right_diff, diffLeft: diffFull.left_diff}                
            #
            diff
          end
          
          
          ###
          
          def _to_db hIn
            flattenDBRes = {}
            result = {nodes: {}, rels: {}}
            hIn[:perf] = {}
            #
            nrModelDB = ReBro::Microservices::Archestry::Vsm::Catman5.buildDBModel(model: hIn[:model], perf: hIn[:perf], shape: hIn[:shape])
            res = { status: 200, perf: hIn[:perf], result: nrModelDB[:result]}
          end            
          

          def _to_modeller hIn
            
            model = hIn[:model][:result]
            result = Hashie::Mash.new 
            preMapping = jsonFile2Mash(currRbFile: __FILE__, jsonFile: "#{Env::Srm::Catman5.info._path}/5_data/modeller/mappings/_remap.json")
      #                   localTypeMapping = Hashie::Mash.new
      #                   localTypeMapping.nodes = preMapping.nodes.invert
      #                   localTypeMapping.rels = preMapping.rels.invert
            localTypeMapping = preMapping       
            #
            result[:metamodel] = model[:metamodel]
            result[:general] = model[:general]
            result[:layers] = model[:layers]              
            result[:counts] = "needs recount"
            result[:nodes] = {}
            result[:rels] = {}
            #
            ReBro::MMP.nodeIterator(model) do |hIterator|
              
              if localTypeMapping.nodes.key?("#{hIterator[:nodeClass]}-#{hIterator[:nodeOrder]}-#{hIterator[:nodeFamily]}-#{hIterator[:nodeGenus]}")
                if localTypeMapping.nodes["#{hIterator[:nodeClass]}-#{hIterator[:nodeOrder]}-#{hIterator[:nodeFamily]}-#{hIterator[:nodeGenus]}"] == false
                  addNode = false
                else
                  mapped = localTypeMapping.nodes["#{hIterator[:nodeClass]}-#{hIterator[:nodeOrder]}-#{hIterator[:nodeFamily]}-#{hIterator[:nodeGenus]}"].split('-')
                  nodeClassMapped  = mapped[0]
                  nodeOrderMapped  = mapped[1]
                  nodeFamilyMapped = mapped[2]
                  nodeGenusMapped  = mapped[3]
                end
              else
                nodeClassMapped  = hIterator[:nodeClass]
                nodeOrderMapped  = hIterator[:nodeOrder]
                nodeFamilyMapped = hIterator[:nodeFamily]
                nodeGenusMapped  = hIterator[:nodeGenus]
              end  
              
              if addNode == false
                hIterator[:nodes].each do |uuid, nodeSet|
                  model[:views].each do |viewID, viewProps|
                    viewProps.nodes.elements.include?(uuid) ? viewProps.nodes.elements.delete(uuid) : true
                  end
                end                  
              else
                result[:nodes][nodeClassMapped] ||= {}
                result[:nodes][nodeClassMapped][nodeOrderMapped] ||= {}
                result[:nodes][nodeClassMapped][nodeOrderMapped][nodeFamilyMapped] ||= {}
                result[:nodes][nodeClassMapped][nodeOrderMapped][nodeFamilyMapped][nodeGenusMapped] ||= {}
              
                hIterator[:nodes].each do |uuid, nodeSet|
                  result[:nodes][nodeClassMapped][nodeOrderMapped][nodeFamilyMapped][nodeGenusMapped][uuid] = nodeSet                 
                end
              end
              
            end
          
            
            model[:rels].each do |type, rels|
              mRelType = localTypeMapping.rels[type]
              #
              if mRelType && mRelType != false
                result[:rels][mRelType] ||= {}
                rels.each do |relUUID, relProps|
                  result[:rels][mRelType][relUUID] = relProps
      #                     result[:rels][mRelType][relUUID][:fType] = localTypeMapping.nodes[relProps[:fType]]
      #                     result[:rels][mRelType][relUUID][:tType] = localTypeMapping.nodes[relProps[:tType]]
                end
              end
              #
            end
            #
            result[:views] = model[:views]
            #
            res = {status: 200, result: result}
            res
          end
          
          def db_transpose__modeller hIn 
            
            result = {nodes: {aaf: {srm: {}}}, rels: {}}
            hIn[:model][:result]['nodes'].each do |node|
              
              h = {
                "eRefID": node['name'],
                "name": node['description'],
                "doc": ""
              }
              
              result[:nodes][:aaf][:srm][node['type']] ||= {}
              result[:nodes][:aaf][:srm][node['type']][node['guid']] = h
            end
            
            hIn[:model][:result]['rels'].each do |rel|
              
            end
            
            
            res = {status: 200, result: result}
          end                        
            
          ## PROTECTED
            private
            
      
            def invoice_private
              puts 222
            end         
            

        
        end
      end
    end
  end
end
