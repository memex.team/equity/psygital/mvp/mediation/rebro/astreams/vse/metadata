# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Env
  module Srm::Catman5
    class << self     
      
      def info
        res = Canvas.new   
        res._p1 = 'srm'
        res._p2 = 'catman5'
        baseBath = "#{File.dirname(File.expand_path(__FILE__))}/../../../../"
        res._funded   = "#{baseBath}/fun/bro/#{res._p1}/#{res._p2}"
        res._financed = "#{baseBath}/fin/bro/#{res._p1}/#{res._p2}"
        #res._class = "#{res._p1.capitalize}::#{res._p2.capitalize}".constantize
        res
      end           
      
      def confluence
        res = Canvas.new    
        res._url  = ENV['Srm_Catman5_Confluence_URL']
        res._user = ENV['Srm_Catman5_Confluence_User']
        res._pass = ENV['Srm_Catman5_Confluence_Password']
        res._token = ENV['Srm_Catman5_Confluence_Token']        
        res
      end
      
      def postgresDB(stage)
        res = Canvas.new    
        #
        case stage
        when :dev
          res._host    = ENV.fetch('Srm_CatMan5_PostgresDBHostDev', 'localhost')
          res._port    = ENV.fetch('Srm_CatMan5_PostgresDBPortDev', '7433')
          res._dbname  = ENV.fetch('Srm_CatMan5_PostgresDBNameDev')       
        when :test
          res._host    = ENV.fetch('Srm_CatMan5_PostgresDBHostTest', 'localhost')
          res._port    = ENV.fetch('Srm_CatMan5_PostgresDBPortTest', '7433')
          res._dbname  = ENV.fetch('Srm_CatMan5_PostgresDBNameTest')
        when :demo
          res._host    = ENV.fetch('Srm_CatMan5_PostgresDBHostDemo', 'localhost')
          res._port    = ENV.fetch('Srm_CatMan5_PostgresDBPortDemo', '7433')
          res._dbname  = ENV.fetch('Srm_CatMan5_PostgresDBNameDemo')
        when :prod
          res._host    = ENV.fetch('Srm_CatMan5_PostgresDBHostProd', 'localhost')
          res._port    = ENV.fetch('Srm_CatMan5_PostgresDBPortProd', '7433')
          res._dbname  = ENV.fetch('Srm_CatMan5_PostgresDBNameProd')
        
        end     
        #
        res._user    = ENV.fetch('Srm_CatMan5_PostgresDBUser')
        res._pass    = ENV.fetch('Srm_CatMan5_PostgresDBPassword')          
        #
        res
      end        
      
    end
  end
end
