# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded  
    module Design::Deck
      class Transpose < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Canvas::ParamBuilder
        namespace "/" do
          
          
        #### GET ####
          ## DESC
          desc 'Transpose', {
            summary: 'Transpose models',
            is_array: true,
            detail: 'handler for transposing models',
            tags: [Env::Srm::Catman5.info._class.to_s]
            }
                          
          ## EXEC 
          get '/:from/:to' do
            classPath = Design::Сables::Transpose 
            res = classPath.send("#{params._apiPath._from}__#{params._apiPath._to}".to_sym, params)
            #
            result ({ status: res[:status].to_i, res: res[:result].to_h, env: env })              
          end                
        end    
          

        
      end
    end
  end
end
