# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded  
    module Design::Deck
      class Diff < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Canvas::ParamBuilder
        namespace "/" do
          
        #### GET ####
          ## DESC
          desc 'Diff SRM', {
            summary: 'Diff SRM coming from Confluence & DB',
            is_array: true,
            detail: 'Get confluence SRM, transpose to DB. Get DB SRM. Diff using left and right diffs.',
            tags: [Env::Srm::Catman5.info._class.to_s]
            }
                        
          
          get '/:from/:to' do
            diff = ReBro::Microservices::Archestry::Vsm::Catman5::Diff.send("diff__#{argsIn[:from]}__#{argsIn[:to]}".to_sym, params: params)
            #
            status 200
            res = diff
            #
            res
          end  
          
        end

      end
    end
  end
end
