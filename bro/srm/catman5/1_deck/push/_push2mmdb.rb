# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded  
    module Design::Deck
      class Push < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Canvas::ParamBuilder
        namespace "/" do
          
          
        #### GET ####
          ## DESC
          desc 'Push from Confluence to MMDB', {
            summary: 'Push from Confluence to MMDB',
            is_array: true,
            detail: 'Call confluence-exporter && call MMDB POST',
            tags: [Env::Srm::Catman5.info._class.to_s]
            }
                          
          ## EXEC 
          
          get '/srm/' do
            h = ReBro::Microservices::Archestry::Vsm::Catman5::Transpose.send("transpose__confluence__db".to_sym, {params: params}) 
            c = ReBro::Connectors::Elasticsearch.connect(ReBro::Env.elasticsearch)
            res = ReBro::Connectors::Elasticsearch.storeMMP(mmp: h[:nrModel], c: c, indexGen: :default)                  
            status res[:status].to_i
            #
            res[:result]
          end            
          
          #
        end

        
      end
    end
  end
end
