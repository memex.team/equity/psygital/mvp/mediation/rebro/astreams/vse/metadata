# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded  
    module Design::Deck
      class Push < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Canvas::ParamBuilder
        namespace "/" do
            
          #### GET ####
            ## DESC
            desc 'Push from Confluence to DB', {
              summary: 'Push from Confluence to DB',
              is_array: true,
              detail: 'Call confluence-exporter, transpose to db model, check by JSON-schema & call DB Import procedure',
              tags: [Env::Srm::Catman5.info._class.to_s]
              }
                            
            ## EXEC 
            get '/db' do
              
              params._apiArgs._shape = 'db'
              transpose = Design::Сables::Transpose.confluence__db(params)
              if transpose[:status] == 200
                res = Financed::Design::Pylons::Db.callDbJsonProcedures(dbModel: transpose[:result], params: params, procName: 'import_status_model')
              else
                res = transpose
              end
              status res[:status].to_i
              #
              res
            end
            
          #### POST ####
            ## DESC
            desc 'Push from MMP JSON to DB', {
              summary: 'Push from MMP JSON to DB',
              is_array: true,
              detail: 'Get MMP JSON, transpose to db model, check by JSON-schema & call DB Import procedure',
              tags: ['SRM::Push']
              }
                            
            ## EXEC                 
            post '/db' do
              res = ReBro::Microservices::Archestry::Vsm::Catman5.send("actions__json2db".to_sym, params)                  
              status res[:status].to_i
              #
              res[:result]
            end   
          end

        
      end
    end
  end
end
