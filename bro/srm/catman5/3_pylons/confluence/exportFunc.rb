# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded    
    module Design::Pylons
      class Confluence
        class << self

          ## PUBLIC
          def confluenceParsing hIn
            #
            confluenceEnv = Env::Srm::Catman5.confluence
            conn = ReBro::Connectors::Confluence.login(url:      confluenceEnv._url, 
                                                      username: confluenceEnv._user,
                                                      password: confluenceEnv._pass)
            #
            # Screen
            hIn._mmp._stAc  = ReBro::Connectors::Confluence.getPageTableAsMappedJSON(conn: conn, 
                                                                                  token:   confluenceEnv._token,                                                                                     
                                                                                  pageID:  hIn._screenPageID, 
                                                                                  tableID: hIn._screenTableID,
                                                                                  mapping: ::CanvasFast.jsonFileCanvas(currRbFile: __FILE__, jsonFile: "#{Env::Srm::Catman5.info._financed}/7_findata/confluence/mappings/statusTableMapping_#{hIn._statusType}.json"),
                                                                                  statusType: hIn._statusType,
                                                                                  valuePreprocClass: Bro::Srm::Catman5::Funded::Design::Pylons::Confluence)
            #
            # Roles 
            rolesRaw = ReBro::Connectors::Confluence.getPageTableAsMappedJSON(conn:   conn, 
                                                                              token:  confluenceEnv._token,                                                                              
                                                                              pageID: hIn._rolesPageID,
                                                                              tableID: hIn._rolesTableID,
                                                                              mapping: ::CanvasFast.jsonFileCanvas(currRbFile: __FILE__, jsonFile: "#{Env::Srm::Catman5.info._financed}/7_findata/confluence/mappings/rolesTableMapping.json"),
                                                                              valuePreprocClass: Bro::Srm::Catman5::Funded::Design::Pylons::Confluence)
            rolesRaw2HashRes = rolesRaw2Hash(rolesRaw)
            hIn._mmp._roles = Canvas.new rolesRaw2HashRes
            #
          end       
          
          
          def valuePreproc(k, v)
            case k.to_sym 
            when :acID
              value = v.strip.gsub(/DEFINITION/, '')
            when :entityName
              value = []
              v.split(',').each do |a|
                value << a.strip.gsub(/\W+/, '')
              end
            when :actionName, :statusFromName, :statusToName # :functionalRoleName,
              v == '-' ? value = v : value = v.strip.gsub(/\W+/, '')
            else
              begin
                value = v.strip
              rescue
                value = v
              end
            end
            #              
            value
          end
          


          ## refactor -> hIn
          def mainGuards(entry)
            case
            when entry._statusFromName == ''
              e = "statusFromName '#{entry._statusFromName}' содержит невалидный ID"
              p e
              ReBro::MMP.fail("EXPORT FAIL: #{e}")
            when entry._statusToName == '' && entry._statusToID != '-'
              e = "statusToName '#{entry}' содержит невалидный ID"
              p e
              ReBro::MMP.fail("EXPORT FAIL: #{e}")
            when entry._actionName == ''
              e = "Действие '#{entry._actionDesc}' ('#{entry._relID}' == '#{entry._statusFromName}' -> '#{entry._statusToName}') содержит невалидный Name '#{entry._actionName}'"      
              p e
              ReBro::MMP.fail("EXPORT FAIL: #{e}")
            when entry._actionID == ''
          #     p "Действие '#{entry._actionDesc}' ('#{entry._relID}' == '#{entry._statusFromName}' -> '#{entry._statusToName}') содержит невалидный ID '#{entry._actionID}'"
              entry._actionID = 'none'
            end
          end
            
          ## PROTECTED
          private

          ## refactor -> hIn
          def rolesRaw2Hash(rolesRaw)
            res = {}
            res[:user] = {}
            res[:func] = {}
            res[:func2user] = {}
            
            rolesRaw.each do |roleRaw|
              roleRaw.each do |roleName, roleValue|
                case roleName
                when /^ur__name/
                  res[:user][roleValue] = roleRaw['ur__desc']
                  res[:func2user][roleValue] ||= [] unless res[:func2user].key?(roleValue)
                when /^fr__/
                  funcRoleName = roleName.split('__')[1]
                  res[:func][funcRoleName] = true
                  if roleValue == '+'                    
                    res[:func2user][roleRaw['ur__name']] << funcRoleName
                  end
                end
                #
              end
            end
            #
            res
          end        

        end
      end
    end
  end
end
