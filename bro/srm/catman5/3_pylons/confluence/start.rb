# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded    
    module Design::Pylons
      class Confluence
        class << self

          ## PUBLIC
          def confluenceExport hIn
            #
            perf = Canvas.new
            perf.a_start = Time.now.to_f
            #
            hIn[:scope] = 'full' unless hIn.key?(:scope)
            #
            args = { argsIn: hIn,  mmp: Canvas.new, perf: perf }
            run = confluenceExportRun(Canvas.new args)
            
            args = { status: 200, perf: run[:perf], result: run }
            res = Canvas.new args                           
            #
            res
          end
      
          
          def confluenceExportRun hIn
            #
            screen2PageMapping = ::CanvasFast.jsonFileCanvas(currRbFile: __FILE__, jsonFile: "#{Env::Srm::Catman5.info._financed}/7_findata/confluence/mappings/pagesMapping.json")
            rolesPageID = screen2PageMapping._roles._pageID.to_s
            rolesTableID = screen2PageMapping._roles._tableID
            nrModel  = ReBro::MMP.makeMMPObject(fqnClass: :vse,  fqnOrder: :metadata, fqnFamily: :srm, fqnGenus: :catman5)
            
            #
            if hIn._argsIn.key?("screenID")
              screenID = screen2PageMapping._screens[hIn._argsIn._screenID.to_s]
              confluenceWorkflowRun(argsIn: hIn._argsIn, 
                            nrModel: nrModel,
                            mmp: hIn._mmp, 
                            screenPageID: screenID.pageID.to_s, 
                            screenTableID: screenID.tableID, 
                            rolesPageID: rolesPageID, 
                            rolesTableID: rolesTableID)
            else
              pagesGet = {}
              screen2PageMapping._screens.each do |statusType, statusTypeProps|
                statusTypeProps.each do |screenID, screenProps|
                  pageID = screenProps._pageID.to_s
                  
                  Log::App.info(false, "Start processing pageID #{pageID} (screen #{screenID}")
                  
                  if pagesGet.key?(pageID) && pagesGet[pageID].include?(screenProps._tableID)
                    Log::App.info(false, "Already got pageID #{pageID} with table #{screenProps._tableID}")
                  else 
                    pagesGet[pageID] ||= []
                    pagesGet[pageID] << screenProps._tableID

                    args = {  argsIn: hIn._argsIn, 
                              nrModel: nrModel,                                             
                              mmp: hIn._mmp, 
                              screenPageID: pageID, 
                              screenTableID: screenProps._tableID, 
                              rolesPageID: rolesPageID, 
                              rolesTableID: rolesTableID,
                              statusType: statusType
                          }
                    confluenceWorkflowRun(Canvas.new args)
                    Log::App.info(false, "PageID #{pageID} (screen #{screenID} is loaded")
                  end
                end
              end
            end
            #
            ReBro::MMP.countModelElements(nrModel)
            ReBro::MMP.checkSchema(nrModel)
            ReBro::Adv.jsonDump(nrModel, 'bdas_srm')   
            #
            nrModel
          end
                        
          
          def confluenceWorkflowRun hIn
            confluenceParsing hIn
            #
            args = { oHash: hIn._nrModel, mmp: hIn._mmp, argsIn: hIn._argsIn, statusType: hIn._statusType }
            Mmp.makeNRV_StatusModel(Canvas.new args)
          end
            
          ## PROTECTED
            private

        end
      end
    end
  end
end
