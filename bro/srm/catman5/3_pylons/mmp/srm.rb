# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded    
    module Design::Pylons
      class Mmp
        class << self

          ## PUBLIC
          def makeNRV_StatusModel hIn
            
            nodeClass  = :aaf
            nodeOrder = :srm
            
            #refO = Canvas.new
            
            hIn._mmp._roles._func.each do |roleName, _none|
              o = Canvas.new
              o._nClass     = nodeClass
              o._nOrder     = nodeOrder
              o._nFamily    = :role
              o._nGenus     = :function
              o._uuidGen   = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{roleName}"
              o._eRefID    = roleName
              o._name      = roleName
              o._doc       = ''
              props        = {'ScreenID' => 'all'}
              o._props     = Canvas.new props
              m           = Canvas.new
              m._skipExisting = true        
              args = {oHash: hIn._oHash, o: o, mod: m}
              ReBro::MMP::Nodes.uuidGen(Canvas.new args)
            end            

            hIn._mmp._roles._user.each do |roleName, roleDesc|
              o = Canvas.new
              o._nClass     = nodeClass
              o._nOrder     = nodeOrder
              o._nFamily    = :role
              o._nGenus     = :user
              o._uuidGen  = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{roleName}"
              o._eRefID    = roleName
              o._name     = roleDesc
              o._doc      = ''
              props       = {'ScreenID' => 'all'}
              o._props    = Canvas.new props
              m           = Canvas.new                
              m._skipExisting = true          
              args = {oHash: hIn._oHash, o: o, mod: m}            
              ReBro::MMP::Nodes.uuidGen(Canvas.new args)
            end
            
            
            hIn._mmp._stAc.each do |entry|
              #
              entry = ::Canvas.new entry
              Confluence.mainGuards(entry)
              #  
              
              ##NODES
              #
              o = Canvas.new
              o._nClass     = nodeClass
              o._nOrder     = nodeOrder
              o._nFamily    = :status
              o._nGenus     = hIn._statusType.to_sym
              o._uuidGen   = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{entry._statusFromName}"
              o._eRefID    = entry._statusFromName
              o._name     = entry._statusFromDesc
              o._doc      = entry._comment
              props       = {'StageID' => entry._statusFromID, 'ScreenID' => entry._screenID}            
              o._props    = Canvas.new props
              m          = Canvas.new
              m._skipExisting = true
              args = { oHash: hIn._oHash, o: o, mod: m }
              ReBro::MMP::Nodes.uuidGen(::Canvas.new args)   
              #
              unless entry._statusToID == '-'
                o = Canvas.new
                o._nClass     = nodeClass
                o._nOrder     = nodeOrder
                o._nFamily    = :status
                o._nGenus     = hIn._statusType.to_sym
                o._uuidGen   = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{entry._statusToName}"
                o._eRefID    = entry._statusToName
                o._name     = entry._statusToDesc
                o._doc      = entry._comment
                props       = {'StageID' => entry._statusToID, 'ScreenID' => entry._screenID}              
                o._props    = Canvas.new props
                m          = Canvas.new
                m._skipExisting = true
                args = { oHash: hIn._oHash, o: o, mod: m }
                ReBro::MMP::Nodes.uuidGen(Canvas.new args)   
              end
              #
              o = Canvas.new
              o._nClass     = nodeClass
              o._nOrder     = nodeOrder
              o._nFamily    = :action
              o._nGenus     = :main
              o._uuidGen  = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{entry._actionName}"
              o._eRefID    = entry._actionName
              o._name     = entry._actionDesc
              o._doc      = entry._relID
              props       = {'StageID' => entry._actionID, 'ScreenID' => entry._screenID}            
              o._props    = Canvas.new props
              m          = Canvas.new
              m._skipExisting = true
              args = {oHash: hIn._oHash, o: o, mod: m}            
              ReBro::MMP::Nodes.uuidGen(Canvas.new args)  
              #
              o = Canvas.new
              o._nClass     = nodeClass
              o._nOrder     = nodeOrder
              o._nFamily    = :screen
              o._nGenus     = :main            
              o._uuidGen  = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{entry._screenID}"
              o._eRefID    = entry._screenID
              o._name     = entry._screenID
              o._doc      = ''
              props        = {'StageID' => entry._screenID, 'ScreenID' => entry._screenID}
              o._props    = Canvas.new props
              m          = Canvas.new
              m._skipExisting = true
              args = {oHash: hIn._oHash, o: o, mod: m}            
              ReBro::MMP::Nodes.uuidGen(Canvas.new args)    
              #
              entry._entityName.each do |entity|
                o = Canvas.new
                o._nClass     = nodeClass
                o._nOrder     = nodeOrder
                o._nFamily    = :entity
                o._nGenus     = :main
                o._uuidGen  = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{entity}"
                o._eRefID    = entity
                o._name     = entity
                o._doc      = ''
                props        = {'ScreenID' => 'all'}                  
                o._props    = Canvas.new props              
                m          = Canvas.new
                m._skipExisting = true
                args = {oHash: hIn._oHash, o: o, mod: m}              
                ReBro::MMP::Nodes.uuidGen(Canvas.new args)   
              end
              #
              o = Canvas.new
              o._nClass     = nodeClass
              o._nOrder     = :business
              o._nFamily    = :ac
              o._nGenus     = :main
              o._uuidGen  = "#{o._nClass}__#{o._nOrder}__#{o._nFamily}__#{o._nGenus}__#{entry._acID}"
              o._eRefID    = entry._acID
              o._name     = entry._acID
              o._doc      = ''
              props        = {'ScreenID' => entry._screenID}            
              o._props    = Canvas.new props
              m          = Canvas.new
              m._skipExisting = true
              args = {oHash: hIn._oHash, o: o, mod: m}            
              ReBro::MMP::Nodes.uuidGen(Canvas.new args)   
              #
            end
            
            
            hIn._mmp._stAc.each do |entry|    
              entry = ::Canvas.new entry
              ## RELS
              #
              o = Canvas.new
              o._uuidGen  = "#{entry._statusFromName}__#{entry._actionName}"
              o._fClass     = nodeClass
              o._fOrder     = nodeOrder
              o._fFamily    = :status
              o._fGenus     = hIn._statusType.to_sym
              o._tClass     = nodeClass                
              o._tOrder     = nodeOrder
              o._tFamily    = :action
              o._tGenus     = :main
              o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{entry._statusFromName}"
              o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{entry._actionName}"
              o._name     = entry._relID
              o._doc      = ''
              args = { oHash: hIn._oHash, o: o, mod: {} }
              ReBro::MMP::Rels.uuidGen(Canvas.new args)     
              #
              unless entry._statusToID == '-'                
                o = Canvas.new
                o._uuidGen  = "#{entry._actionName}__#{entry._statusToName}"
                o._fClass     = nodeClass
                o._fOrder     = nodeOrder
                o._fFamily    = :action
                o._fGenus     = :main
                o._tClass     = nodeClass
                o._tOrder     = nodeOrder
                o._tFamily    = :status
                o._tGenus     = hIn._statusType.to_sym
                o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{entry._actionName}"
                o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{entry._statusToName}"
                o._name     = entry._relID
                o._doc      = ''
                args = { oHash: hIn._oHash, o: o, mod: {} }
                ReBro::MMP::Rels.uuidGen(Canvas.new args) 
                #
                ### !!! DERIVED
                o = Canvas.new
                o._uuidGen  = "#{entry._statusFromName}__#{entry._statusToName}"
                o._fClass     = nodeClass
                o._fOrder     = nodeOrder
                o._fFamily    = :status
                o._fGenus     = hIn._statusType.to_sym
                o._tClass     = nodeClass
                o._tOrder     = nodeOrder
                o._tFamily    = :status
                o._tGenus     = hIn._statusType.to_sym
                o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{entry._statusFromName}"
                o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{entry._statusToName}"
                o._name     = entry._relID
                o._doc      = ''
                args = { oHash: hIn._oHash, o: o, mod: {} }
                ReBro::MMP::Rels.uuidGen(Canvas.new args)   
                ###                  
              end
              #
              #
              o = Canvas.new
              o._uuidGen  = "#{entry._actionName}__#{entry._acID}"
              o._fClass     = nodeClass
              o._fOrder     = nodeOrder
              o._fFamily    = :action
              o._fGenus     = :main
              o._tClass     = nodeClass
              o._tOrder     = :business
              o._tFamily    = :ac
              o._tGenus     = :main
              o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{entry._actionName}"
              o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{entry._acID}"
              o._name     = entry._relID
              o._doc      = ''
              args = { oHash: hIn._oHash, o: o, mod: {} }
              ReBro::MMP::Rels.uuidGen(Canvas.new args)  
              #
              entry._entityName.each do |entity|
                o = Canvas.new
                o._uuidGen  = "#{entry._actionName}__#{entity}"
                o._fClass     = nodeClass
                o._fOrder     = nodeOrder
                o._fFamily    = :action
                o._fGenus     = :main
                o._tClass     = nodeClass
                o._tOrder     = nodeOrder
                o._tFamily    = :entity
                o._tGenus     = :main
                o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{entry._actionName}"
                o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{entity}"
                o._name     = entry._relID
                o._doc      = ''
                args = { oHash: hIn._oHash, o: o, mod: {} }
                ReBro::MMP::Rels.uuidGen(Canvas.new args)      
              end
              #
              o = Canvas.new
              o._uuidGen  = "#{entry._statusFromName}__#{entry._screenID}"
              o._fClass     = nodeClass
              o._fOrder     = nodeOrder
              o._fFamily    = :status
              o._fGenus     = hIn._statusType.to_sym
              o._tClass     = nodeClass
              o._tOrder     = nodeOrder
              o._tFamily    = :screen
              o._tGenus     = :main
              o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{entry._statusFromName}"
              o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{entry._screenID}"
              o._name     = entry._relID
              o._doc      = ''
              args = { oHash: hIn._oHash, o: o, mod: {} }
              ReBro::MMP::Rels.uuidGen(Canvas.new args)    
              #
              entry._functionalRoleName.split("\n").each do |fRole|
                fRole.strip!
                o = Canvas.new
                o._uuidGen  = "#{fRole}__#{entry._actionName}"
                o._fClass     = nodeClass
                o._fOrder     = nodeOrder
                o._fFamily    = :role
                o._fGenus     = :function
                o._tClass     = nodeClass
                o._tOrder     = nodeOrder
                o._tFamily    = :action
                o._tGenus     = :main
                o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{fRole}"
                o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{entry._actionName}"
                o._name     = fRole
                o._doc      = ''
                args = { oHash: hIn._oHash, o: o, mod: {} }
                ReBro::MMP::Rels.uuidGen(Canvas.new args)  
              end
                    
            end
            
            hIn._mmp._roles._func2user.each do |userRoleName, funcRoles|
              funcRoles.each do |funcRoleName|
                o = Canvas.new
                o._uuidGen  = "#{userRoleName}__#{funcRoleName}"
                o._fClass     = nodeClass
                o._fOrder     = nodeOrder
                o._fFamily    = :role
                o._fGenus     = :user
                o._tClass     = nodeClass
                o._tOrder     = nodeOrder
                o._tFamily    = :role
                o._tGenus     = :function
                o._fUuidGen = "#{o._fClass}__#{o._fOrder}__#{o._fFamily}__#{o._fGenus}__#{userRoleName}"
                o._tUuidGen = "#{o._tClass}__#{o._tOrder}__#{o._tFamily}__#{o._tGenus}__#{funcRoleName}"
                o._name     = funcRoleName
                o._doc      = ''
                args = { oHash: hIn._oHash, o: o, mod: {} }
                ReBro::MMP::Rels.uuidGen(Canvas.new args)    
              end
            end              
            #
            allViews = []
            hIn._oHash._nodes.each do |nodeClass, nodeOrderSet|
              nodeOrderSet.each do |nodeOrder, nodeFamilySet|
                nodeFamilySet.each do |nodeFamily, nodeGenusSet|
                  nodeGenusSet.each do |nodeGenus, nodes|

                    nodes.each do |uuid, nodeProps|
                      if nodeProps._props.key?('ScreenID')
                        if nodeProps._props._ScreenID == 'all'
                          allViews << uuid
                        else
                          args = { name: nodeProps._props._ScreenID, oHash: hIn._oHash, mod: {} }
                          viewUuid = makeViews(Canvas.new args)
                          
                          args = { oHash: hIn._oHash, view: viewUuid, node: uuid }
                          addNodeToView(Canvas.new args)
                        end
                      else
                        p "Element is out of view: #{nodeClass}::#{nodeOrder}::#{nodeFamily}::#{nodeGenus} - #{uuid} - #{nodeProps._name}"
                      end
                    end
                  end
                end
              end
            end
            #
            hIn._oHash._views.each do |view, props|
              allViews.each do |node|
                props._nodes[node] = {}
              end
            end
            
            #
            
            #p hIn._oHash[:nodes]
            
          end
          
          
          def makeViews hIn
            o = Canvas.new
            o._uuidGen  = hIn._name
            o._eRefID   = hIn._name
            o._name     = hIn._name
            o._doc      = hIn._name
            o._props    = {}
            m           = Canvas.new
            m._skipExisting = true
            args = { oHash: hIn._oHash, o: o,  mod: m }
            res = ReBro::MMP::Views.uuidGen(Canvas.new args)
            #
            res
          end

          def addNodeToView hIn
            hIn._oHash._views[hIn._view]._nodes._elements[hIn._node]
          end
            
          ## PROTECTED
            private

        end
      end
    end
  end
end
