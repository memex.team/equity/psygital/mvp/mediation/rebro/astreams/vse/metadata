# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Srm::Catman5
  module Funded    
    module Design::Pylons
      class Db
        class << self

          ## PUBLIC
          def map2DB_StatusModel hIn
            
            localTypeMapping = ::CanvasFast.jsonFileCanvas(currRbFile: __FILE__, jsonFile: "#{Env::Srm::Catman5.info._funded}/5_data/db/mappings/_remap.json")
            #
            res = Canvas.new
            #
            
            ReBro::MMP.nodeIterator(hIn._model._result) do |hIterator|
              
              addNode = true                
              fullFQN = "#{hIterator._nodeClass}-#{hIterator._nodeOrder}-#{hIterator._nodeFamily}-#{hIterator._nodeGenus}"
              
              #Log::App.debug("fullFQN: #{Log::App.debug(fullFQN)}")
              #Log::App.debug("localTypeMapping._nodes: #{localTypeMapping._nodes}")

              if localTypeMapping._nodes.key?(fullFQN)
                if localTypeMapping._nodes[fullFQN] == "@skip"
                  addNode = false
                else
                  fullFQNmapped = localTypeMapping._nodes[fullFQN].split('-')
                  nodeClassMapped  = fullFQNmapped[0]
                  nodeOrderMapped  = fullFQNmapped[1]
                  nodeFamilyMapped = fullFQNmapped[2]
                  nodeGenusMapped  = fullFQNmapped[3]
                end
              else
                nodeClassMapped  = hIterator._nodeClass
                nodeOrderMapped  = hIterator._nodeOrder
                nodeFamilyMapped = hIterator._nodeFamily
                nodeGenusMapped  = hIterator._nodeGenus
              end     
              
              if addNode == true
                hIterator._nodes.each do |uuid, nodeProps|
                  nodeGenusMapped == 'main' ? nodeType = nodeFamilyMapped.to_s : nodeType = "#{nodeFamilyMapped}_#{nodeGenusMapped}"
                  
                  if nodeProps == 'skip' or nodeProps == 'other'
                    true
                  else
                    res._nodes[nodeType] ||= {}
                    #
                    n = Canvas.new
                    n._guid = uuid.to_s
                    if nodeProps.key?('props') && nodeProps._props.key?('StageID')
                      n._id = nodeProps._props._StageID
                    else
                      n._id = "none"
                    end
                    n._type = nodeType
                    n._name = nodeProps._eRefID
                    n._description = nodeProps._name
                    nodeProps._eStatus == nil ? n._status = "" : n._status = nodeProps._eStatus
                    #
                    res._nodes[nodeType][uuid.to_s] = n
                  end
                end
              end
            end
              

            ReBro::MMP.relIterator(hIn._model._result) do |hIterator|
              
              addRel = true                                
              
              if localTypeMapping._rels.key?(hIterator._from) && localTypeMapping._rels[hIterator._from].key?(hIterator._to)
                if localTypeMapping._rels[hIterator._from][hIterator._to] == "@skip"
                  addRel = false
                else
                  fullFQNmapped = localTypeMapping._rels[hIterator._from.to_s][hIterator._to.to_s]
                  nodeFromMapped  = fullFQNmapped._from
                  nodeToMapped  = fullFQNmapped._to
                end
              else
                nodeFromMapped  = hIterator._from.to_s
                nodeToMapped  = hIterator._to.to_s
              end               
            
              if addRel == true
                fTypeArray = nodeFromMapped.split('__')
                fTypeArray[3] == 'main' ? fType = fTypeArray[2] : fType = "#{fTypeArray[2]}_#{fTypeArray[3]}"
                tTypeArray = nodeToMapped.split('__')
                tTypeArray[3] == 'main' ? tType = tTypeArray[2] : tType = "#{tTypeArray[2]}_#{tTypeArray[3]}"
                
                hIterator._rels.each do |uuid, relProps|
                  r = Canvas.new                   
                  r._source = relProps._fUUID
                  r._sourceType = fType
                  r._destination = relProps._tUUID
                  r._destinationType = tType
                  relProps._eStatus == nil ? r._status = "" : r._status = relProps._eStatus
                  
                  #
                  res._rels[fType][tType][uuid.to_s] = r
                end
              end
            end
            #
            res
          end
            
          ## PROTECTED
            private

        end
      end
    end
  end
end
