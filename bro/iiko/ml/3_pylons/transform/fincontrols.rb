# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Iiko::Ml
  module Funded  
    module Design::Pylons
      class Transform
        class << self
                                
          def outgoingInvoice2fincontrols hIn   
            args = {apiPath: {dataType: 'suppliers', transformType: 'fincontrols'}}
            suppliers = Design::Сables::Etl.transform(Canvas.new args)
            #
            args = {apiPath: {dataType: 'stores', transformType: 'fincontrols'}}
            stores = Design::Сables::Etl.transform(Canvas.new args)
            
            res = Canvas.new
            hIn._document.each do |doc|
              h = Canvas.new

              supplierKey = "_#{doc._counteragentId.gsub('-', '_')}" unless doc._counteragentId == nil
              h._supplier = suppliers[supplierKey] if supplierKey
              
              h._date = DateTime.parse(doc._dateIncoming).strftime('%d.%m.%Y')
              h._docNum = doc._documentNumber
              
              if doc.key?('items')
                items = doc._items._item
                item = []
                
                if items.is_a?(Canvas)
                  args = { item: items, stores: stores }
                  item << mappingIncomingInvoiceItems(Canvas.new args)
                elsif items.is_a?(Array)
                  items.each do |itemEach|
                    args = { item: itemEach, stores: stores }
                    item << mappingIncomingInvoiceItems(Canvas.new args)
                  end
                end
                h._items = item
              else
                pp 11
              end                  
              
              res["_#{doc._id.gsub('-', '_')}"] = h
            end
            #
            res
          end
          
          def suppliers2fincontrols hIn    
            res = Canvas.new
            hIn._employee.each do |supplier|
              h = Canvas.new
              h._name = supplier._name.to_s
              supplier._code.class == Canvas ? h._code = 'none' : h._code = supplier._code.to_s
              
              supplier._taxpayerIdNumber.is_a?(Saxerator::Builder::StringElement) ? h._taxpayerIdNumber = supplier._taxpayerIdNumber.to_s : h._taxpayerIdNumber = 'none'
              res["_#{supplier._id.gsub('-', '_')}"] = h            
            end
            #
            res
          end    
          
          def stores2fincontrols hIn    
            res = Canvas.new
            hIn._corporateItemDto.each do |store|
              h = Canvas.new
              h._name = store._name.to_s
              h._code = store._code.to_s
              res["_#{store._id.gsub('-', '_')}"] = h
            end
            #
            res
          end                 
          
          def mappingIncomingInvoiceItems hIn
            h = Canvas.new
            h._priceSum = hIn._item._sum.to_f.round(2) 
            h._vatSum = hIn._item._vatSum.to_f.round(2) 
            h._priceSumCalc = (h._priceSum  - h._vatSum).round(2)
            h._vatPercent = hIn._item._vatPercent.to_i
            vatPercentMap = {'20' => 'oc', '10' => 'pb', '0' => 'pa' }                  
            h._vatPercentMap = vatPercentMap[h._vatPercent.to_s]
            
            #Log::App.debug(hIn._item._storeId)
            h._store = hIn._stores["_#{hIn._item._storeId.gsub('-', '_')}"]
            #
            h
          end

        end
      end
    end
  end
end
