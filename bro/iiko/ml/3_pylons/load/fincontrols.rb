# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Iiko::Ml
  module Funded  
    module Design::Pylons
      class Load
        class << self
          
          DELIMITER = ';'
                                
          def outgoingInvoice2fincontrols hIn   

            res = []
            
            h = Canvas.new
            h._h1  = 'Номер документа'   #D1
            h._h2  = 'Счет'              #D2
            h._h3  = 'Сумма'             #D3
            h._h4  = 'Центр прибыли'     #D4
            h._h5  = 'Текст'               #--
            h._h6  = 'Код налога'        #D6
            h._h7  = 'Спец.ГК'             #--
            h._h8  = 'Код проводки'      #D8
            h._h9  = 'База налога'         #--
            h._h10 = 'Базовая дата платежа'#--
            h._h11 = 'Условие платежа'     #--
            h._h12 = 'сумма налога'        #--
            h._h13 = 'МВЗ'                 #--
            h._h14 = 'Присвоение'        #D14
            h._h15 = 'Валюта'              #--
            h._h16 = 'Order'               #--
            h._h17 = 'Amount in Local Currency' #--
            h._h18 = 'БЕ'                #D18
            h._h19 = 'Дата документа'    #D19
            h._h20 = 'Дата проводки'     #D20
            h._h21 = 'Текст Header'      #D21
            h._h22 = 'ССылка'            #D22
            h._h23 = 'Контрольный счет'    #--
            h._h24 = 'Тип банка-партнера'  #--
            h._h25 = 'Месяц финансового года'#--
            h._h26 = 'Валютный курс'       #--
            h._h27 = 'СПП-элемент'         #--
            h._h28 = 'Номер договора - RE' #--
            h._h29 = 'Дата пересчета'      #--
            h._h30 = 'Текст назначения платежа' #--
            h._h31 = 'Получатель платежа / плательщик' #--
            h._h32 = 'XREF1'             #D32
            h._h33 = 'XREF2'               #--
            h._h34 = 'XREF3'               #--
            h._h35 = 'ПФМ'                 #--
            h._h36 = 'Вид документа'     #D36
            h._h37 = 'В/договора'          #--
            h._h38 = 'Код для блокировки платежа' #--
            h._h39 = 'Сумма налога 2'      #--
            h._h40 = 'Подробный текст позиции' #--
            h._h41 = 'Статус плательщика'  #--
            h._h42 = 'KPP: подраздел'      #--
            h._h43 = 'Код бюджета'         #--
            h._h44 = 'Код ОКАТО'           #--
            h._h45 = 'Причина платежа'     #--
            h._h46 = 'Налоговый период'    #--
            h._h47 = 'Номер документа'     #--
            h._h48 = 'Дата докумен.'       #--
            h._h49 = 'Тип платежа'         #--
            a = []
            h.to_h.reverse_each {|k, v| a << v }
            res << a.join(DELIMITER)
            
            doc = {}
            pos = 1
            hIn.each do |invoiceID, invoice|
              invoice._items.each do |item|         
                doc[invoiceID] ||= {}
                doc[invoiceID][:d] ||= []
                doc[invoiceID][:c] ||= {}
                doc[invoiceID][:c][item._vatPercentMap] ||= []
                
                args = { item: item, invoice: invoice, account: 2309100, position: pos }
                doc[invoiceID][:d] << incomingInvoice2SAP_generate(Canvas.new args)
                
                args = { item: item, invoice: invoice, account: 5401220, position: pos }
                doc[invoiceID][:c][item._vatPercentMap] << incomingInvoice2SAP_generate(Canvas.new args)
              end                   
              pos += 1                    
            end
            
            
            aggr = {}
            doc.each do |invoiceID, invoice|
              aggr[invoiceID] ||= {}
              invoice.each do |type, preitems|
                aggr[invoiceID][type] ||= {}                        
                              
                case type               
                when :d
                  priceSumCalc = 0
                  preitems.each do |item|
                
                    priceSumCalc = priceSumCalc + item._d3
                  end
                  aggr[invoiceID][type][:cumulative] = preitems[0].to_h
                  aggr[invoiceID][type][:cumulative][:d3] = priceSumCalc.round(2) 
                
                when :c
                  preitems.each do |vatGroup, items|
                    priceSumCalc = 0                          
                    items.each do |item|                          
                      priceSumCalc = priceSumCalc + item._d3
                    end
                    aggr[invoiceID][type][vatGroup] = items[0].to_h
                    aggr[invoiceID][type][vatGroup][:d3] = priceSumCalc.round(2)                           
                  end
                  
                end
              end
            end
            
            aggr.each do |invoiceID, invoice|
              invoice.each do |type, vatGroups|
                vatGroups.each do |vatGroup, items|
                  f = []
                  h.to_h.reverse_each do |headerKey, headerValue|
                    headerKeyID = headerKey.to_s.split('h')[1].to_i
                    itemsKey = "d#{headerKeyID}"
                    items.key?(itemsKey) ? f << items[itemsKey] : f << ''                            
                  end
                  res << f.join(DELIMITER)
                end
              end
            end
            
            #
            res
          end
                  
                  
          def incomingInvoice2SAP_generate hIn
            case hIn._account
            when 2309100
              accCode = 40
            when 5401220
              accCode = 50
            else
              accCode = 'error'
            end
            
            r = Canvas.new
            r._d1 = hIn._position #Номер документа
            r._d2 = hIn._account #Счет
            r._d3 = hIn._item._priceSumCalc #Сумма
            r._d4 = hIn._item._store._name #Центр прибыли
            
            accCode == 40 ? r._d6 = "" : r._d6 = hIn._item._vatPercentMap #Код налога
            
            r._d8 = accCode
            r._d14 = hIn._invoice._docNum #Присвоение
            r._d18 = 'RU63' #БЕ
            r._d19 = hIn._invoice._date #Дата документа
            r._d20 = DateTime.now.strftime('%d.%m.%Y')
            r._d21 = 'Приходная накладная' #Текст Header
            r._d22 = hIn._invoice._docNum #ССылка
            r._d32 = hIn._invoice._supplier._taxpayerIdNumber #XREF1
            r._d36 = 'SA' #Вид документа   
            
            r
          end

        end
      end
    end
  end
end
