# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Iiko::Ml
  module Funded  
    module Design::Pylons
      class Extract
        class << self
                                
          def incomingInvoice hIn
            if hIn._apiArgs.key?(:fromYMD) && hIn._apiArgs.key?(:toYMD)          
              url = "#{Env::Iiko::Ml.iiko._url}/documents/export/incomingInvoice?key=#{hIn._accessToken}&from=#{hIn._apiArgs._fromYMD}&to=#{hIn._apiArgs._toYMD}"
              res = HTTP.get(url).to_s                
            else
              res = "Please fill fromYMD, toYMD" 
            end
            #
            res
          end
          
          def outgoingInvoice hIn
            url = "#{Env::Iiko::Ml.iiko._url}/documents/export/outgoingInvoice?key=#{hIn._accessToken}&from=#{hIn._apiArgs._fromYMD}&to=#{hIn._apiArgs._toYMD}"
            res = HTTP.get(url).to_s                
            #
            res
          end                
          
          def departments hIn
            url = "#{Env::Iiko::Ml.iiko._url}/corporation/departments/?key=#{hIn._accessToken}"
            res = HTTP.get(url).to_s                
            #
            res
          end    
          
          def suppliers hIn
            url = "#{Env::Iiko::Ml.iiko._url}/suppliers/?key=#{hIn._accessToken}"
            res = HTTP.get(url).to_s                
            #
            res
          end      
          
          def stores hIn
            url = "#{Env::Iiko::Ml.iiko._url}/corporation/stores/?key=#{hIn._accessToken}"
            res = HTTP.get(url).to_s                
            #
            res
          end                 
          
          def sales hIn
            if hIn.key?(:fromDMY) && hIn.key?(:toDMY) && hIn.key?(:departmentID)
              url = "#{Env::Iiko::Ml.iiko._url}/reports/sales?key=#{hIn._accessToken}&dateFrom=#{hIn._apiArgs._fromDMY}&dateTo=#{hIn._apiArgs._toDMY}&department=#{hIn._apiArgs._departmentID}"
              res = HTTP.get(url).to_s      
            else
              res = "Please fill fromDMY, toDMY, departmentID"
            end
            #Log::App.debug(false, "Res: #{res}")
            #
            res
          end   
          
          def olapV1Fields hIn
            a = []
            hIn._apiArgs._groupRow.split(',').each do |row|
              a << "&groupRow=#{row}"
            end
            
            url = "#{Env::Iiko::Ml.iiko._url}/reports/olap?key=#{hIn._accessToken}&report=#{hIn._apiArgs._report}&from=#{hIn._apiArgs._fromDMY}&to=#{hIn._apiArgs._toDMY}#{a.join()}"
            Log::App.debug(false, url)            
            res = HTTP.get(url).to_s                
            #
            res
          end      
          
          def olapV2Fields hIn
            url = "#{Env::Iiko::Ml.iiko._url}/v2/reports/olap/columns?key=#{hIn._accessToken}&reportType=#{hIn._apiArgs._reportType}"
            Log::App.debug(false, url)            
            res = HTTP.get(url).to_s                
            #
            res
          end           

        end
      end
    end
  end
end
