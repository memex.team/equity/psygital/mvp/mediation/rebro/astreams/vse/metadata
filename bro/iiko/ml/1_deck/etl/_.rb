# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Iiko::Ml
  module Funded  
    module Design::Deck
      class Etl < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Canvas::ParamBuilder
        namespace "/" do        
          
          
        #### GET ####
          ## DESC
          desc 'IIko connector', {
            summary: 'IIko connector',
            is_array: true,
            detail: 'Call IIko connector & export to JSON',
              tags: [Env::Iiko::Ml.info._class.to_s]
            }
                          
          ## EXEC 
          get '/extract/:dataType' do
            res = Design::Сables::Etl.extract(params)
            content_type 'application/json'
            res                  
          end
          
          get '/transform/:dataType/:transformType' do
            res = Design::Сables::Etl.transform(params)
            content_type 'application/json'
            res.to_h                  
          end
          
          get '/load/:dataType/:transformType' do
            res = Design::Сables::Etl.load(params)
            #
            if params._apiPath.key?(:format) 
              case  params._apiPath._format
              when 'csv', 'txt'
                content_type 'text/csv'
                stream Grape::StreamGenCSVSimple.new data: res
              when 'xlsx'
                content_type 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                stream Grape::StreamGenCSV2XLSX.new data: res                      
              else
                content_type 'application/json'
                res
              end
            end            
          end

          #
        end

      end
    end
  end
end

