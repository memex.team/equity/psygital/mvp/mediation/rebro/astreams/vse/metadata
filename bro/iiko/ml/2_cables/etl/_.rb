# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Iiko::Ml
  module Funded  
    module Design::Сables
      class Etl
        class << self

          ## PUBLIC
          def getIikoToken
            authUrl = "#{Env::Iiko::Ml.iiko._url}/auth?login=#{Env::Iiko::Ml.iiko._user}&pass=#{Env::Iiko::Ml.iiko._passHash}"
            accessToken = HTTP.get(authUrl).to_s
            #
            accessToken
          end      
          
          
          def extract hIn
            Log::App.debug(false, "## Сables::extract started")
            hIn._accessToken = getIikoToken
            xml = Design::Pylons::Extract.send(hIn._apiPath._dataType.to_sym, hIn)
            begin
              res = Saxerator.parser(xml).all.symbolize_keys!
            rescue 
              res = xml
            end
            Log::App.debug(false, "## Сables::extract finished")
            #
            res
          end
          
          
          def transform hIn
            #Log::App.debug(false, "## Сables::transform started")
            extractRes = extract hIn
            extractRes = Canvas.new extractRes
            res = Design::Pylons::Transform.send("#{hIn._apiPath._dataType}2#{hIn._apiPath._transformType}".to_sym, extractRes)
            Log::App.debug(false, "## Сables::transform finished")
            #
            res
          end   
          
          
          def load hIn
            Log::App.debug(false, "## Сables::load started")
            transform = transform hIn
            res = Design::Pylons::Load.send("#{hIn._apiPath._dataType}2#{hIn._apiPath._transformType}".to_sym, transform)
            Log::App.debug(false, "## Сables::load finished")
            #
            res
          end               
            
          ## PROTECTED
            private
            
      
            def d
              puts 222
            end         
            

        
        end
      end
    end
  end
end
