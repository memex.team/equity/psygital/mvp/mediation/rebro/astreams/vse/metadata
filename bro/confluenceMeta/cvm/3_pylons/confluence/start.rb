# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::ConfluenceMeta::Cvm
  module Funded  
    module Design::Pylons
      class Confluence
        class << self

          def export hIn
            res = runExport(hIn)
            #
            res
          end       
          
          def valuePreproc(k, v)
            v
          end        
            
          ## PRIVATE
          private

          def runExport hIn
            #
            screen2PageMapping = ::CanvasFast.jsonFileCanvas(currRbFile: __FILE__, jsonFile: "#{Env::ConfluenceMeta::Cvm.info._path}/5_data/confluence/mappings/pagesMapping.json")
            pageType = "_#{hIn._apiPath._pageType}".to_sym
            pageID = screen2PageMapping._pages.send(pageType)._pageID.to_i
            #
            confluenceEnv = Env::ConfluenceMeta::Cvm.confluence
            conn = ReBro::Connectors::Confluence.login(url:      confluenceEnv._url, 
                                                      username: confluenceEnv._user,
                                                      password: confluenceEnv._pass)
            #
            # Screen
            res = ReBro::Connectors::Confluence.getPageTableAsMappedJSON( conn:    conn, 
                                                                          pageID:  pageID, 
                                                                          token:   confluenceEnv._token,
                                                                          tableID: 0,
                                                                          mapping: ::CanvasFast.jsonFileCanvas(currRbFile: __FILE__, jsonFile: "#{Env::ConfluenceMeta::Cvm.info._path}/5_data/confluence/mappings/statusTableMapping.json"),
                                                                          mode:   :fullPageTable,
                                                                          dataCoverage:     :withMissing,
                                                                          valuePreprocClass: Bro::ConfluenceMeta::Cvm::Funded::Design::Pylons::Confluence,
                                                                        )

            res
          end          
            
        end
      end
    end
  end
end
