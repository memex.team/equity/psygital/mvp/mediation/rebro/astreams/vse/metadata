# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::ConfluenceMeta::Cvm
  module Funded  
    module Design::Deck
      class Export < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Canvas::ParamBuilder
        namespace "/" do        
          
          
        #### GET ####
          ## DESC
          desc 'Convert from Confluence to JSON', {
            summary: 'Convert from Confluence to JSON',
            is_array: true,
            detail: 'Call confluence-exporter & export to JSON',
              tags: [Env::ConfluenceMeta::Cvm.info._class.to_s]
            }
                          
          ## EXEC 
          get '/:pageType' do
            res = Design::Pylons::Confluence.export(params)
            result ({ status: 200, res: res, env: env })              
          end

          #
        end

      end
    end
  end
end
