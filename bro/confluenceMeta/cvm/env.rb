# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######


module Env
  module ConfluenceMeta::Cvm
    class << self     
      
      def info
        res = Canvas.new    
        res._path = "#{File.dirname(File.expand_path(__FILE__))}"
        res._class = ConfluenceMeta::Cvm
        classA = res._class.to_s.split("::")
        res._p1 = classA[1].downcase
        res._p2 = classA[2].downcase       
        res
      end           
      
      def confluence
        res = Canvas.new    
        res._url  = ENV['ConfluenceMeta_Cvm_Confluence_URL']
        res._user = ENV['ConfluenceMeta_Cvm_Confluence_User']
        res._pass = ENV['ConfluenceMeta_Cvm_Confluence_Password']
        res._token = ENV['ConfluenceMeta_Cvm_Confluence_Token']
        res
      end
      
    end
  end
end
