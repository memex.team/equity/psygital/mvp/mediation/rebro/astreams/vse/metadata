# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

Bundler.require(:parsers, :webscraper, :rdbms_pg, :workflow, :rubyparser, :orchestrator, :k8s, :math)
require 'confluence-rest-api'

class Canvas < CanvasFast; end


module Env; 
  module ConfluenceMeta; end;
  module Srm; end;
  module Iiko; end;
end


module Bro
  module ConfluenceMeta; module Cvm; module Funded; module Design; end; end; end; end
  module Srm; module Catman5; 
    module Funded;   module Design; end; end;
    module Financed; module Design; end; end; 
  end; end
  module Iiko; module Ml; module Funded; module Design; end; end; end; end
end

module Env
  module Base
    class << self
   
      LOCALSTORAGE = LMDB.new('/tmp/', { mapsize: 100000000, maxdbs: 30})
      
      def apiDesc
        res = {
            title: 'Metadata API',
            description: 'Metadata API',
            contact_name: 'Eugene Istomin',
            contact_email: 'e.istomin@memex.team',
            license: 'See License.md',
          }
        #
        res
      end
            
      def logLevel
        res = Canvas.new
        res._dev = Logger::DEBUG
        res._prod = Logger::INFO
        res
      end
      
      
      def api
        res = Canvas.new
        res._path = '/api'
        res
      end   
      
      def localStorage
        LOCALSTORAGE
      end
      
    end
  end
end



#wBody = Env::Base.wBody
#wBody.each do |wStream, banks|
  ##module Env
  ##moduleCall = "#{wStream.capitalizeFirst}".constantize
  #module Env; send("module".to_sym, "#{wStream.capitalizeFirst}".constantize)
  #end; end

  ##module.send(moduleCall, "end")
  ##; end

  #banks.each do |bank, bridgeElements|
    #bridgeElements.each do |bridgeElement, elementTypes|
      #elementTypes.each do |elementType, _p|
        #Log::App.debug("Waterbody::#{wStream.capitalizeFirst}::#{bank.capitalizeFirst}::Bridge::#{bridgeElement.capitalizeFirst}::#{elementType.capitalizeFirst}")
      #end
    #end
  #end
#end

